<!-- MODALES -->
<div class="modal fade" id="HTMLModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <ul class="list-group">
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Introducción al HTML
              <span class="badge badge-primary badge-pill">2 hrs.</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Formatos de página
              <span class="badge badge-primary badge-pill">2 hrs.</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Formatos de texto
              <span class="badge badge-primary badge-pill">1hrs.</span>
            </li></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Imágenes
              <span class="badge badge-primary badge-pill">1hrs.</span>
            </li></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Listas
              <span class="badge badge-primary badge-pill">3hrs.</span>
            </li></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Tablas
              <span class="badge badge-primary badge-pill">3hrs.</span>
            </li></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Formularios
              <span class="badge badge-primary badge-pill">2hrs.</span>
            </li></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Estilos
              <span class="badge badge-primary badge-pill">3hrs.</span>
            </li></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Grillas con frameworks
              <span class="badge badge-primary badge-pill">4hrs.</span>
            </li></span>
            </li>
          </ul>

          <div class="alert alert-success" role="alert">
            21 horas de contenido
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>